# Use the latest build from the development branch

Currently controls-kt is under active development. If you want to use the latest build from the development branch, you can do the following steps:

1. Clone the repository:
    ```bash
    git clone -b dev https://git.sciprog.center/kscience/controls-kt.git 
    ```
2. Change the [version](https://git.sciprog.center/kscience/controls-kt/src/commit/5b655a9354de5ddb4be25ee9e6be876f14f10b87/build.gradle.kts#L16) in your `build.gradle.kts` file to new unique:
    ```kotlin
    allprojects {
        // ...
        version = "0.3.0-dev-6-19-02-24-local"
        // ...
    }
    ```
3. Execute `publishToMavenLocal` task:
    ```bash
    ./gradlew publishToMavenLocal
    ```
    or from Idea Gradle panel:

    ![publishToMavenLocal](./images/publishToMavenLocal.png)
    
    Latest version of the library will be published to your local maven repository.
4. In your project add mavenLocal() to your repositories:
    ```kotlin
    repositories {
        mavenLocal()
        mavenCentral()
        // ...
    }
    ```
5. Add the dependencies you need using the unique version you set in p2:
    ```kotlin
    // ...
    dependencies {
        implementation("space.kscience:controls-core:0.3.0-dev-6-19-02-24-local")
        // ...
    }
    // ...
    ```


