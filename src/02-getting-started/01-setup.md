# Setting up the project
For quick start we provide a minimal working [boilerplate](https://github.com/kapot65/controls-kt-boilerplate) project with controls-kt. We are going to use it as a base for our tutorial. Clone it with the following command:
```shell
git clone https://github.com/kapot65/controls-kt-boilerplate.git
```
After cloning you will have a project structure like this:
```
controls-kt-boilerplate/
├── build.gradle.kts
├── gradle.properties
├── README.md
├── settings.gradle.kts
└── src
    ├── commonMain
    │   └── kotlin
    │       └── devices
    │           └── IDemoDevice.kt
    ├── jsMain
    │   ├── kotlin
    │   │   └── Main.kt
    │   └── resources
    │       └── index.html
    └── jvmMain
        └── kotlin
            ├── devices
            │   └── DemoDevice.kt
            └── Server.kt
```
This is a common kotlin mutliplatform project with enabled `jvm` and `js` targets and some controls-kt dependencies. Boilerplate code contains a simple demo device and server with embed device manager. Let's take a look at the project structure:
- ./src/commonMain/kotlin/devices/IDemoDevice.kt - contains a demo device interface and specification of its properties and actions.
- ./src/jvmMain/kotlin/devices/DemoDevice.kt - contains a demo device implementation.
- ./src/jvmMain/kotlin/Server.kt file contains a magix server with embed device manager with installed demo device.
- ./src/jsMain/kotlin/Main.kt - contains a simple controls-kt client example.

Before we start, let's make some refactoring:
1. change project name to `attractor` in `settings.gradle.kts`  and in project folder name.
2. change js bundle name in ./src/jsMain/resources/index.html to `attractor.js`
3. Change `IDemoDevice` to `IAttractor`, and `DemoDevice` to `Attractor` (Shift-F6 in Idea).

After refactoring, the project structure will look like this:
```
attractor/
├── build.gradle.kts
├── gradle.properties
├── README.md
├── settings.gradle.kts
└── src
    ├── commonMain
    │   └── kotlin
    │       └── devices
    │           └── IAttractor.kt
    ├── jsMain
    │   ├── kotlin
    │   │   └── Main.kt
    │   └── resources
    │       └── index.html
    └── jvmMain
        └── kotlin
            ├── devices
            │   └── Attractor.kt
            └── Server.kt
```
To test the prepared project, you can follow the steps below:
1. start Server.kt (Ctrl-Shift-F10 from Idea)
2. start client
   ```shell
   ./gradlew jsBrowserRun
   ```
   or from Idea gradle panel
3. check browser console to see device property changes
   
Server terminal should print `tick` messages every second. Browser console should print continiously updated property values.

<!-- add conclusion -->
