# Create client
We have an attractor device running on the server. Now we need to create a client to visualize the attractor.

## Preparation
In this page we focus on communication with magix. So we are going to use basic html graphics with plotly as a graph library. For complex ui we recommend to use [compose-multiplatform](https://www.jetbrains.com/lp/compose-multiplatform/) or [visionforge](https://git.sciprog.center/kscience/visionforge).
<div class="warning">
Currently visionforge is actively developed and not ready for production use. Documentation is not ready yet.
</div>

Let's put our ui into `index.html`:
```html
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
        <script src="https://cdn.plot.ly/plotly-2.29.1.min.js" charset="utf-8"></script>
    </head>
    <body>
    <div id="plot"></div>
    <script>
        function plot(x, y) {
            Plotly.newPlot('plot', [{
                x, y, mode: 'lines'
            }], { title: 'Attractor' });
        }
    </script>
    <button id="reset">Reset</button>
    <br>
    <input type="number" step="0.01" value="2.5" id="a_value">
    <button id="a_update">update</button><div id="a_current"></div>
    <script src="attractor.js"></script>
    </body>
</html>
```
It has a static plot widget to display attractor, js function to plot data, and some input fields to control `a` property and reset the attractor. 

We need some glue code to interact with ui from kotlin.
To use `plot(x, y)` we define it as external function:
```kotlin
external fun plot(x: Array<Double>, y: Array<Double>)
```
To connect to button events we will use `addEventListener`:
```kotlin
document.getElementById("reset")!!.addEventListener("click", {
    // ...
})
```

Finally we clear original `main` function leaving only endpoint initialization and add plot state variables:
```kotlin
suspend fun main(): Unit = coroutineScope {
    // connect to Magix
    val sendEndpoint = MagixEndpoint.rSocketWithWebSockets("localhost")
    val buffer = mutableListOf<Coords>()
    fun MutableList<Coords>.pushRing(ele: Coords) {
        if (buffer.size > 10) this.removeFirst()
        this += ele
    }
}
```

## Connect to device
controls-kt uses two different approaches to control devices from client side: by single propety flows or by whole device. We are going to use both of them.
<div class="warning">
    Currently both approaches are not ready. There is some functionality you can use only by one of them. That is the reason we need to use both of them for now. In future releases we are going to make them equal.
</div>

### Approach 1: Connect by controlsPropertyFlow
This approach is used to connect to single property of the device. It is designed to be used in cases when you need only little number of device properties on client side. We are going to use it for live update of device `coords` and `a` property.
<div class="warning">
Currently only read/write property is supported. No actions.
</div>
<!-- пояснить про доступ по спеке -->

For our needs we will use [MagixEndpoint.controlsPropertyFlow](https://git.sciprog.center/kscience/controls-kt/src/commit/2946f23a4bffd2bfd6dda6df1f618df46c4fc3d6/controls-magix/src/commonMain/kotlin/space/kscience/controls/client/DeviceClient.kt#L136) method. It creates a typed flow for desired device property. We will use it to connect to `coords` and `a` properties of attractor.
For `coords` flow we will use tranformations below:
```kotlin
launch {
    sendEndpoint.controlsPropertyFlow(
        "controls-kt", Name.of("demo"), IAttractor.coords).collect { coord ->
        buffer.pushRing(coord)
        plot(buffer.map { it.x } .toTypedArray(), buffer.map { it.y } .toTypedArray())
    }
}
```
Each update of `coords` property pushed to handmade ring buffer. Then data from buffer is converted to plotly format and plotted.

To connect `a` to ui we use next transformation:
```kotlin
launch {
    sendEndpoint.controlsPropertyFlow("controls-kt", Name.of("demo"), IAttractor.a).collect {
        document.getElementById("a_current")!!.textContent = "curr: $it"
    }
}
```
Each `a` update is simply overwrites current corresponding div text value.

We start both flows inside `launch` scope to prevent them from blocking main coroutine.


### Approach 2: Connect by device
This approach is used to connect to whole device. It is designed to be used in cases when heavily use of device properties and actions is needed. We are going to write `a` changes and execute `reset` action.

First we need to create [DeviceClient](https://git.sciprog.center/kscience/controls-kt/src/commit/2946f23a4bffd2bfd6dda6df1f618df46c4fc3d6/controls-magix/src/commonMain/kotlin/space/kscience/controls/client/DeviceClient.kt#L26) instance:
```kotlin
val device = run {
    val context = Context("clientContext") {}
    val device = sendEndpoint.remoteDevice(
        context,
        "controls-kt",
        "controls-kt",
        Name.of("demo")
    )
    device
}
```
To do it we need to create `Context` instance and use it with [MagixEndpoint.remoteDevice](https://git.sciprog.center/kscience/controls-kt/src/commit/2946f23a4bffd2bfd6dda6df1f618df46c4fc3d6/controls-magix/src/commonMain/kotlin/space/kscience/controls/client/DeviceClient.kt#L115) method.

Now we have `DeviceClient` instance which stores device address. This is a general class and it does not have access to actual `Attractor` class methods. Same as with `controlsPropertyFlow` we must access to device properties and actions through device specification.

Together with glue code it will look like:
```kotlin
document.getElementById("reset")!!.addEventListener("click", {
    buffer.clear()
    launch { device.execute(IAttractor.reset) }
})
```
for reset action (we clear buffer and execute `reset` action) and
```kotlin
document.getElementById("a_update")!!.addEventListener("click", {
    launch {
        val newValue = document.getElementById("a_value").unsafeCast<HTMLInputElement>().value.toDouble()
        device.write(IAttractor.a, newValue)
    }
})
```
for write `a` property (we read value from input field and write it to `a` property).

## Conclusion
Full client code will look like:
```kotlin
external fun plot(x: Array<Double>, y: Array<Double>)
suspend fun main(): Unit = coroutineScope {
    val sendEndpoint = MagixEndpoint.rSocketWithWebSockets("localhost")
    val buffer = mutableListOf<Coords>()
    fun MutableList<Coords>.pushRing(ele: Coords) {
        if (buffer.size > 10) this.removeFirst()
        this += ele
    }
    val device = run {
        val context = Context("clientContext")
        val device = sendEndpoint.remoteDevice(
            context, "controls-kt", "controls-kt",
            Name.of("demo")
        )
        device
    }
    document.getElementById("reset")!!.addEventListener("click", {
        buffer.clear()
        launch { device.execute(IAttractor.reset) }
    })
    document.getElementById("a_update")!!.addEventListener("click", {
        launch {
            val newValue = document.getElementById("a_value").unsafeCast<HTMLInputElement>().value.toDouble()
            device.write(IAttractor.a, newValue)
        }
    })
    launch { sendEndpoint.controlsPropertyFlow(
            "controls-kt", Name.of("demo"), IAttractor.coords).collect { coord ->
            buffer.pushRing(coord)
            plot(buffer.map { it.x } .toTypedArray(), buffer.map { it.y } .toTypedArray())
    } }
    launch { sendEndpoint.controlsPropertyFlow("controls-kt", Name.of("demo"), IAttractor.a).collect {
            document.getElementById("a_current")!!.textContent = "curr: $it"
    } }
}
```
To run it you can use `jsBrowserRun` gradle task. It will start a server and open a browser window with your client.
To update client code you can use `jsBrowserDevelopmentWebpack` task while `jsBrowserRun` is running or just rerun `jsBrowserRun` task.
`Server.kt` must be running to provide data for client.
In browser you should see:
![web-ui](./images/web-ui.png)
You can play with ui to check how it works.


<!-- add conclusion -->