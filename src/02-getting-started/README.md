This chapter contains step-by-step tutorial teaching you how to use basic controls-kt functionality:
- define device specification (device properties, actions)
- create controls-kt event loop
- read/write device properties
- execute device actions

During this chapter we are going to create a simple device that simulates a simple attractor in 2D space defined by the following differential equations:
```
dx/dt = y - ax^2
dy/dt = -x - y
```
<!-- Добавить слов про задачу -->
The device will have the following properties:
- (x, y) - coordinates as current step
- a - parameter of the attractor
and the following action:
- reset - reset the coordinates

  
## Prerequisites
- Tutorial assumes that you use IntelliJ IDEA as your IDE as currently it's the only way to work with Kotlin Multiplatform projects.
<!-- - Examples in tutorial is based on [controls-kt-experiments](https://github.com/kapot65/controls-kt-experiments) project.  -->
    
<!-- You can clone it and follow the tutorial or use your own project. (переделать)-->

<!-- Придумать простое демо (желательно поинтереснее SinCos) устройство для этой секции -->