# Attach to Magix
Now that we have created a device, its time to attach it to Magix loop. Actually, `Server.kt` from boilerplate already all we need, so let's just take a look at it:
    
```kotlin
suspend fun main(): Unit = coroutineScope {
    // intialize Device Manager
    val context = Context("clientContext") {
        plugin(DeviceManager)
    }
    val manager = context.request(DeviceManager)
    val device = Attractor.build(context, Meta.EMPTY)
    manager.install("demo", device)
    device.onOpen()

    // start Magix Loop
    startMagixServer(buffer = 20)

    // attach Device Manager to Magix Loop
    run {
        val magixEndpoint = MagixEndpoint.rSocketWithWebSockets("localhost")
        manager.launchMagixService(magixEndpoint)
    }
}
```
- First block of code initializes DeviceManager with Attractor device. To do this, we need to create a context and request DeviceManager plugin from it. Then we create an instance of Attractor device and install it to DeviceManager.
    <div class="warning">
    currently we must call `onOpen` method manually. This will be fixed in future releases.
    </div>
- Then we start Magix loop with `startMagixServer` function. By default, it will start Magix loop on RSocket with WebSockets support on [default](https://git.sciprog.center/kscience/controls-kt/src/commit/5b655a9354de5ddb4be25ee9e6be876f14f10b87/magix/magix-api/src/commonMain/kotlin/space/kscience/magix/api/MagixEndpoint.kt#L33) port.
  - We set buffer size to 20 to get pretty traces on the client side.
    <!-- написать поподробнее про буффер (мб на отдельной странице) -->
  - You can tune magix loop [plugins](../04-advanced/magix-plugins.md), but it is not necessary for now.
  - Magix loop is just a server, it can be initialized in separate binary. We embed it in the same binary for convenience.
- In the last block of code, we attach DeviceManager to Magix loop. We are connecting to loop via RSocket with WebSockets support (`rSocketWithWebSockets`) like we do it in any other Magix client.

<!-- add conclusion -->