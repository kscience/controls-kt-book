## Device structure
Let's take a look at device implementation. You can find it in 
`./src/commonMain/kotlin/devices/IAttractor.kt` and `./src/jvmMain/kotlin/devices/Attractor.kt` files.


Structurally, a controls-kt device consists of three entities: device interface, device implementation and device specification.
- device interface - a kotlin interface for device implementation.
    ```kotlin
    interface IAttractor : Device {
        var timeScaleState: Double
        var sinScaleState: Double

        fun sinValue(): Double

        companion object : DeviceSpec<IAttractor>() {
            // ...
        }
    }
    ```
    It serves as a bridge between implementation and specification.
    <div class="warning">
    Despite of "standart" approach of using interfaces it is not intended for contolling device. You will not be able to use interface fields and methods within controls-kt API.
    </div>
    Device interface must be used to define implementation fields and methods that needed by device specification.

    - device inteface must be placed in `common` in order to be accessible from both `jvm` and `js` targets.
- device specification - an object that contains actual device controlling interface. It exists inside interface companion object.
    ```kotlin
    interface IAttractor : Device {
        // ...
        companion object : DeviceSpec<IAttractor>() {
            val timeScale by mutableProperty(MetaConverter.double, IAttractor::timeScaleState)
            val sinScale by mutableProperty(MetaConverter.double, IAttractor::sinScaleState)

            val sin by doubleProperty { sinValue() }

            val resetScale by unitAction {
                write(timeScale, 5000.0)
                write(sinScale, 1.0)
            }
        }
    }
    ```
    Specification serves as a bridge between device interface and magix loop and used to define device properties and actions. Every property and action defined in specification will be available in magix loop.
- device implementaion - an actual device implementation.
    ```kotlin
        class Attractor(context: Context, meta: Meta) : DeviceBySpec<Attractor>(IAttractor, context, meta), IAttractor {
        override var timeScaleState = 5000.0
        override var sinScaleState = 1.0

        private fun time(): Instant = Instant.now()

        override fun sinValue(): Double = sin(time().toEpochMilli().toDouble() / timeScaleState) * sinScaleState

        companion object : DeviceSpec<IAttractor>(), Factory<Attractor> {
            override fun build(context: Context, meta: Meta) = Attractor(context, meta)
            override suspend fun IAttractor.onOpen() {
                launch {
                    read(IAttractor.sinScale)
                    read(IAttractor.timeScale)
                }
                doRecurring(1.seconds) {
                    println("tick")
                    read(IAttractor.sin)
                }
            }
        }
    }
    ```
    This class should contain all hardware-specific code and lifecycle management. It should be placed in `jvm` or `native` target depending on the platform.

Now let's implement our Attractor device. We will change the initial code given by boilerplate. We start with device interface. 

## Device interface
Interface must contain all fields and methods we want to use in device specification. For our attractor
```
dx/dt = y - ax^2
dy/dt = -x - y
```
we will need:
- `a` - a mutable parameter of the attractor
- `tick() -> Coords(x, y)` - a method that will calculate next step of the attractor
- `reset()` - a method that will reset the attractor coordinates

For convenience we will define Coords as a data class:
```kotlin
@Serializable
data class Coords(val x: Double, val y: Double)
```
for using `@Serializable` annotation we need to add serialization plugin to our `build.gradle.kts` file
```kotlin
plugins {
    kotlin("multiplatform") version "1.9.22"
    kotlin("plugin.serialization") version "1.9.22" // <- add this
}
```
Now we can define our device interface:
```kotlin
interface IAttractor : Device {
    var a: Double
    fun tick(): Coords
    fun reset()
    companion object : DeviceSpec<IAttractor>() {
        // ...
    }
}
```
## Device specification
Now we can define our device specification. We will need to define `a` as a mutable property, coords as read-only property binded to `tick` and `reset` as unit action.
```kotlin
interface IAttractor : Device {
    // ...
    companion object : DeviceSpec<IAttractor>() {
        val a by mutableProperty(MetaConverter.double, IAttractor::a)
        @OptIn(DFExperimental::class)
        val coords by property(MetaConverter.serializable<Coords>()) { tick() }
        val reset by unitAction { reset() }
    }
}
```
Together with interface full code looks like this:
```kotlin
interface IAttractor : Device {
    var a: Double
    fun tick(): Coords
    fun reset()
    companion object : DeviceSpec<IAttractor>() {
        val a by mutableProperty(MetaConverter.double, IAttractor::a)
        @OptIn(DFExperimental::class)
        val coords by property(MetaConverter.serializable<Coords>()) { tick() }
        val reset by unitAction { reset() }
    }
}
```

## Device implementation
Device implementation looks like this:
```kotlin
class Attractor(context: Context, meta: Meta) : DeviceBySpec<Attractor>(IAttractor, context, meta), IAttractor {
    private var coords = Coords(0.1, 0.1)
    override var a = 2.5
    override fun tick(): Coords {
        val dx = coords.y - a * Math.pow(coords.x, 2.0)
        val dy = -coords.x - coords.y
        coords = Coords(coords.x + dx, coords.y + dy)
        return coords
    }
    override fun reset() {
        coords = Coords(0.1, 0.1)
    }
    companion object : DeviceSpec<IAttractor>(), Factory<Attractor> {
        override fun build(context: Context, meta: Meta) = Attractor(context, meta)
        override suspend fun IAttractor.onOpen() {
            doRecurring(1.seconds) {
                println(read(IAttractor.coords))
            }
        }
    }
}
```
1. First we add `coords` - an internal state of our attractor used for other device logic.
2. Then we override `a` and `tick` and `reset` methods defined in interface. `a` bridges to `a` property in specification, `tick` implements numerical solution for the next step of the attractor and `reset` resets the attractor coordinates.
3. Finally, in companion object we define lifecycle methods. `build` is a factory method that creates an instance of our device. `onOpen` is a lifecycle method that is called when device is opened. inside `onOpen` we define a recurring task that reads `coords` every second.

<!-- add conclusion -->
